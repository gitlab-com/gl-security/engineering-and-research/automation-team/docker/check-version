############################
# STEP 1 build executable binary
############################
FROM golang:alpine AS builder

# git is needed to download go dependencies
RUN apk update && apk add --no-cache git
WORKDIR /build
COPY . .

RUN go mod download && go mod verify
RUN GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /build/check-version

FROM alpine:3

# git is executed by the check-version binary
RUN apk update && apk add --no-cache git
COPY --from=builder /build/check-version /bin/check-version

ENTRYPOINT ["/bin/check-version"]
