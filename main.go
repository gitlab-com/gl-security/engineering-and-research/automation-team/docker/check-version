package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"

	"golang.org/x/mod/semver"
)

var defaultBranch = os.Getenv("CI_DEFAULT_BRANCH")

func error(format string, v ...interface{}) {
	fmt.Println(fmt.Sprintf(format, v...))
	os.Exit(1)
}

func runCommand(name string, arg ...string) string {
	cmd := exec.Command(name, arg...)
	cmd.Stderr = os.Stderr
	out, err := cmd.Output()
	if err != nil {
		error(err.Error())
	}

	return strings.TrimSpace(string(out))
}

func main() {
	if defaultBranch == "" {
		error("CI_DEFAULT_BRANCH not set")
	}

	currentVer := runCommand("git", "show", fmt.Sprintf("%s:VERSION", defaultBranch))
	newVer := runCommand("cat", "VERSION")

	if !strings.HasPrefix(currentVer, "v") {
		currentVer = "v" + currentVer
	}
	if !strings.HasPrefix(newVer, "v") {
		newVer = "v" + newVer
	}

	result := semver.Compare(string(currentVer), string(newVer))

	if result == 0 {
		error("VERSION file has not been updated")
	} else if result == 1 {
		error("New version (%s) is less than current version (%s)", newVer, currentVer)
	}
}
