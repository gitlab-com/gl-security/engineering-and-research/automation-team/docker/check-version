# Check Version

This image is used to verify that a `VERSION` file has been updated to a more recent version.

The following is an example of the CI configuration needed to validate version updates. Rules should be
added to this example to ensure that this stage is only run for events that warrant a version bump.

```yaml
check version:
  stage: test
  image:
    name: registry.gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/docker/check-version:latest
    entrypoint: ['']
  script:
    - git fetch origin $CI_DEFAULT_BRANCH:$CI_DEFAULT_BRANCH
    - check-version
```

